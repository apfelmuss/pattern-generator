#!/usr/bin/env python3

import argparse
import os

if __name__ == '__main__':

    parser = argparse.ArgumentParser( description =
                                     "Generate a binary file with a specific pattern as content." )

    parser.add_argument( '--width', type=int, required=True,
                        help="Number of bytes in one word (like one memory line)")

    parser.add_argument( '--words', type=int, required=True,
                        help="Number of words in binary file (width x words = file size in bytes)")

    parser.add_argument( '--byteorder', type=str,
                        choices=[ 'big', 'little' ], default='big',
                        help="For integer patterns specify the order of bytes in one word (default: %(default)s)" )

    parser.add_argument( '--value', type=int, default=0,
                        help="User defined value to fill each word, when pattern 'value' is selected. \
                                Be sure value fits into number of bytes set by option '--width'. \
                                If less bytes needed to encode, then the upper bytes are filled with 0. \
                                If more bytes needed to encode, then an OverflowError raises. \
                                The value must be a decimal number. \
                                (default: %(default)s)")

    parser.add_argument( 'pattern', type=str,
                        choices=[ 'upcounter', 'downcounter', 'zero', 'ones', 'value', 'random' ],
                        help="The pattern which is used as file content (default: %(default)s)" )

    parser.add_argument( 'filename', type=str,
                        help="Name of the generated binary file" )

    args = parser.parse_args()

    with open( args.filename, 'wb' ) as file:

        if args.pattern == 'upcounter':
            for word in range( 0, args.words ):
                file.write( word.to_bytes( args.width, byteorder=args.byteorder ) )

        elif args.pattern == 'downcounter':
            for word in range( args.words-1, -1, -1 ):
                file.write( word.to_bytes( args.width, byteorder=args.byteorder ) )

        elif args.pattern == 'zero':
            for _ in range( args.words ):
                file.write( bytes( args.width ) )

        elif args.pattern == 'ones':
            ones = bytearray()
            for _ in range( args.width ):
                ones.extend( b'\xff' );
            for _ in range( args.words ):
                file.write( ones )

        elif args.pattern == 'value':
            for _ in range( args.words ):
                file.write( args.value.to_bytes( args.width, byteorder=args.byteorder ) )

        elif args.pattern == 'random':
            for _ in range( args.words ):
                file.write( os.getrandom( args.width ) )
